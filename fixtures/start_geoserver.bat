@echo off
cd C:\Geonode\GeoNode\geoserver\data
"%JRE_HOME%\bin\java.exe" -Xmx512m -XX:MaxPermSize=256m  -DGEOSERVER_DATA_DIR=C:\Geonode\geoserverdata  -Dorg.eclipse.jetty.server.webapp.parentLoaderPriority=true  -jar C:\Geonode\GeoNode\downloaded\jetty-runner-8.1.8.v20121106.jar --log C:\Geonode\GeoNode\geoserver\jetty.log  C:\Geonode\GeoNode\scripts\misc\jetty-runner.xml &